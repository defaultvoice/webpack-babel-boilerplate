const path = require('path');

module.exports = {
    // Точка входа
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js',
        // static path
        publicPath: '/static/',
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: { 
                            presets: ['env'] 
                        }
                    }
                ] 
            }
        ]
    }
}